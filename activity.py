name: str= "Jose"
age: int = 38
occupation: str = "writer"
movie: str = "One More Chance"
rating: float = 99.6

print(f"I am {name}, and I am {age} years old. I work as a {occupation}, and my rating for {movie} is {rating}%.")

num1: int = 2
num2: int = 3
num3: int = 4

print(num1 * num2)
print(num1 < num3)

num2 += num3